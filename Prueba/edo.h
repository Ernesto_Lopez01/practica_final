#ifndef __EDO_H
#define __EDO_H

#ifdef __cplusplus
extern "C"{
#endif

#include <stdio.h>
#include <stdlib.h>

typedef enum{
    goNorte,
    waitNorte,
    goEste,
    waitEste
} estados;

typedef enum{
    No_Car,
    Car_Este,
    Car_Norte,
    Car_Ambos
} carros;

int Se_Edo(int car, int edo2);
void Estados(int edo); 

#ifdef __cplusplus
}

#endif

#endif
