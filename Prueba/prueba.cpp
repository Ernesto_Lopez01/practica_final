#include "edo.h"
#include <gtest/gtest.h>

TEST(SquareRootTest, Prueba){

	ASSERT_EQ(goNorte, Se_Edo(No_Car,goNorte));
    ASSERT_EQ(goNorte, Se_Edo(Car_Este,goNorte));
    ASSERT_EQ(waitNorte, Se_Edo(Car_Norte,goNorte));
    ASSERT_EQ(waitNorte, Se_Edo(Car_Ambos,goNorte));

    ASSERT_EQ(goEste, Se_Edo(No_Car,waitNorte));
    ASSERT_EQ(goEste, Se_Edo(Car_Este,waitNorte));
    ASSERT_EQ(goEste, Se_Edo(Car_Norte,waitNorte));
    ASSERT_EQ(goEste, Se_Edo(Car_Ambos,waitNorte));

	ASSERT_EQ(goEste, Se_Edo(No_Car,goEste));
	ASSERT_EQ(goEste, Se_Edo(Car_Este,goEste));    
	ASSERT_EQ(waitEste, Se_Edo(Car_Norte,goEste));
	ASSERT_EQ(waitEste, Se_Edo(Car_Ambos,goEste));

	ASSERT_EQ(goNorte, Se_Edo(No_Car,waitEste));
	ASSERT_EQ(goNorte, Se_Edo(Car_Este,waitEste));
	ASSERT_EQ(goNorte, Se_Edo(Car_Norte,waitEste));
	ASSERT_EQ(goNorte, Se_Edo(Car_Ambos,waitEste));
   
}


int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
